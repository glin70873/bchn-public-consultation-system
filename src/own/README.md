This folder is for source code written by Bitcoin Cash Node project 
for the BCHN PCS.

All sources here must be licensed under AGLPv3 (at least), but can in
certain instances be dual-licensed for inclusion into e.g. MIT-licensed
BCHN code.
