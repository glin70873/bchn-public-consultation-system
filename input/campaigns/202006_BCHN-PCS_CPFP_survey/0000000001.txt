Question 1 - Have you used Child-Pays-For-Parent (CPFP) on BCH?
---------------------------------------------------------------

- [ ] - Yes
- [x] - No


Question 2 - If you answered 'Yes' above, what were/are your use cases?
-----------------------------------------------------------------------

Describe your use cases for CPFP below.

1. Use case: ...
2. Use case: ...
3. Use case: ...

... (fill in and add or remove list items as needed)


Question 3 - Are you planning to use CPFP in the future?
--------------------------------------------------------

- [x] - No plans
- [ ] - Yes, but do not have a specific timeframe
- [ ] - Yes, I'm planning to use it in the next ............... (give timeframe)


Information about yourself
--------------------------

This will help us to better assess the input.
Please put an 'X' or and 'x' into whichever of the following applies:

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [x] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [ ] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [x] - a BCH user
- [ ] - other - please specify: ....................................

