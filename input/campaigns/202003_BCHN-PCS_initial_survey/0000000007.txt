Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. Transparent leadership
2. Open development (basically as-is)
3. Cooperation with other full node teams 

Regarding 3. I think there are benefits to be had in having multiple development teams with multiple organizational structures. For example Flowee is a one-man shop moving fast, while BU has a (sluggish but) strong organization which encourages (internal) discussion. 

Different approaches yeald different point of views that could be valuable (assuming good faith).

Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. On chain scaling (but also off-chain as an option, why not)
2. I see a point in Peter Rizun's proposal for a spec and a default (slow, but simple and correct) reference client (Problem is - who would maintain it)
3. SDKs and intergations... for something like the whole ethereum ecosystem around metamask (it's really something that one cannot appreciate without trying it out)

Section C - Information about yourself
--------------------------------------
This will help us to better assess the input.
Please put an 'X' or and 'x' into whichever of the following applies:

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc) (*I just made a propotype protocol ove BCH, does that count?*)
- [ ] - an executive of a retail business using Bitcoin Cash
- [x] - a person with significant amounts of their long term asset holdings in BCH
- [x] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [x] - a BCH user
- [ ] - other - please specify:
