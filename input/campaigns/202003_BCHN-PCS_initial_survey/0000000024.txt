Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. Quality, stability.  

2. Organizational thoughts   
I's hard to do good creative and design work when the tools are already made. In that case we are limited by them.  
So if we want to create a developer community (maybe) we should create those tools. If it's more like a work setting, then what exists [slack etc.] is fine.  
Is it possible to create a map, a visual map of the protocol? Everyone likes maps.

Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. For people to f***ing notice that this is a fine technology.

Section C - Information about yourself
--------------------------------------

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [ ] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [ ] - a BCH user
- [ ] - other - please specify:
