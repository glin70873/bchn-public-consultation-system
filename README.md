# BCHN Public Consultation System

The BCHN Public Consultation System (short: PCS) is a system to obtain public input to steer project goals and help shape its roadmap.

This repository is structure in the following way:

- doc/ : further documentation about the PCS system, how it works and
  guidelines for its use
- input/ : contains public input into the system, in the form of feedback to
  campaigns
- output/ : contains output of the system, in the form of forms, surveys and reports
- src/ : source code used in processing inputs or outputs
- src/own/ : own source code, licensed under AGLPv3 and where indicated,
  additional licenses
- src/contrib/ : source code contributed by others under specific open source licenses
- README.md : this README file which describes the repository structure and
  important files
- LICENSE.md : overview of which parts of the repository are licensed in which way
