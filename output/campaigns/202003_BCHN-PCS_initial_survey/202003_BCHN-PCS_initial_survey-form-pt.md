Sistema de Consulta Pública do Bitcoin Cash Node - Pesquisa de março de 2020
============================================================================

Esta é uma pesquisa da comunidade pelo projeto Bitcoin Cash Node, para coletar feedback sobre o que você gostaria que acontecesse no projeto BCHN e no Bitcoin Cash como um todo.

Como preencher esta pesquisa
----------------------------

No documento abaixo, você encontrará duas seções principais (A e B) que pediríamos que você preenchesse.

O primeiro (Seção A) é para listar sua "lista de desejos" do que você gostaria que acontecesse no projeto do Full Node do BCHN.

O segundo (Seção B) é sua lista de desejos para o Bitcoin Cash em geral.

Você pode adicionar qualquer número de itens, mas faça o seguinte:
- numere cada lista de 1 ... n 
- classifique os itens na sua lista de acordo com a prioridade decrescente (1 = prioridade mais alta, ... n = prioridade mais baixa)
- para cada item em suas listas, dar tanto uma breve descrição (frase curta ou uma frase) e se você sente que é necessário, uma explicação um pouco mais longo que pode incluir lógica, detalhes técnicos, as dependências que você vê para ele etc.

Não é necessário você fornecer 3 ou mais itens em cada lista - se você puder pensar em menos, exclua os itens não utilizados ou deixe em branco.

A terceira seção (C) é ter uma idéia de quais papéis melhor descrevem como você se vê no ecossistema Bitcoin Cash. Não solicitaremos informações de identificação pessoal e você poderá enviar este formulário de pesquisa anonimamente para nós.


Como enviar esta pesquisa
-------------------------

Existem várias maneiras:

Se você deseja um anonimato completo:
- salve sua resposta em um despejo de texto on-line serviço (sugestões: https://hastebin.com ou https://glot.io/new/plaintext) e transmita o URL da sua resposta salva a qualquer pessoa no projeto BCHN com instruções para encaminhá-lo ao freetrader. Você também pode enviá-lo ao freetrader por meio de qualquer canal conhecido em que você confia - pode ser um email para o freetrader no tuta dot io, ou Memorando (https://memo.cash/topic/BCHN+PCS+input) ou levantando um problema no Gitlab (https://gitlab.com/bitcoin-cash-node/bchn-public-consultation-system/) para registrar sua resposta ou DM para freetrader na folga do Bitcoin Cash Node (bitcoincashnode.slack.com) ou através do pontes IRC de telegrama ou freenode para folga do BCHN). Veja https://bitcoincashnode.org para obter maneiras de entrar em contato com o projeto. Com os métodos listados, você precisará usar um email gravador ou perfil pseudônimo para enviar sua resposta.


Se você não se importa de associar sua resposta a uma identidade já conhecida:
- use qualquer um dos canais de envio acima mencionados com um perfil conhecido. Além disso, você também pode gerenciar sua resposta ao u / ftrader ou enviá-lo em # discussão geral sobre a folga do Bitcoin Cash Node.

Como não temos uma boa maneira de verificar reivindicações de função de envios anônimos, a associação verificável com sua identidade online conhecida pode nos ajudar a priorizar. Prometemos, com o melhor de nossos esforços, manter sua associação de identidade confidencial.

Prazo para envio
----------------

Esta pesquisa está ativa até o final de março de 2020, mas se você enviar seu feedback o mais rápido possível, isso nos ajudaria a incluí-lo em consideração em nossa proposta de financiamento planejamento.

pretendemos realizar pesquisas futuras semelhantes a esta para calibrar o curso de nosso projeto.

`% <--- Começando a seção de feedbacks --- você pode cortar sua resposta aqui ---`

Seção A - O que você deseja ver no projeto de Full Node do Bitcoin Cash Node (BCHN)
-----------------------------------------------------------------------------------

Pode ser qualquer coisa: aprimoramento técnico, organizacional, direcional ...

1.
2.
3.
...
(adicione mais itens conforme necessário)


Seção B - O que você deseja ver no Bitcoin Cash (BCH) em geral
--------------------------------------------------------------

Geralmente coisas que afetam todo o protocolo / moeda / ecossistema,
e não o BCHN especificamente.

1.
2.
3.
...
(adicione mais itens conforme necessário)


Seção C - Informações sobre você
--------------------------------

Isso nos ajudará a avaliar melhor a entrada.
Coloque um 'X' ou 'x' em qualquer uma das seguintes situações:

Você se descreveria como (você pode verificar várias respostas):

- [] - um minerador de Bitcoin Cash ou executivo de mineração

- [] - um pool de Bitcoin Cash operador ou executivo

- [] - um operador ou executivo de troca de Bitcoin Cash

- [] - operador ou executivo de provedor de serviços de pagamento de Bitcoin Cash

- [] - um protocolo de Bitcoin Cash ou desenvolvedor de cliente de nó completo

- [] - um desenvolvedor de aplicativos em camada de Bitcoin Cash (Carteiras SPV, sistemas de token etc.)

- [] - um executivo de uma empresa de varejo usando Bitcoin Cash

- [] - uma pessoa com quantias significativas de seus ativos a longo prazo no BCH

- [] - alguém que fornece liquidez, derivativos e outros serviços financeiros em Bitcoin Cash

- [] - um usuário do BCH

- [] - outro - por favor especifique: ............................. .......

`>% --- Fim da seção de feedback --- você pode cortar sua resposta aqui ---`
