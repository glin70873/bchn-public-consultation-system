Bitcoin Cash Node Public Consultation System - March 2020 Survey
================================================================

This is a community survey by the Bitcoin Cash Node project, to gather feedback on what you would like to see happen in the BCHN project and in Bitcoin Cash as a whole.


How to fill out this survey
---------------------------

In the document below, you will find two main sections (A and B) which we would ask you to fill out.

The first (Section A) is for listing your "wish list" of things you'd like to see happen in the BCHN full node project.

The second (Section B) is your wish list for Bitcoin Cash in general.

You can add any number of items, but please do the following:
- number each list from 1...n 
- rank the items in your list according to descending priority (1 = highest priority, ... n = lowest priority)
- for each item in your lists, give both a very brief description (short phrase or one sentence) and if you feel it is needed, a slightly longer explanation which can include rationale, technical details, dependencies you see for it etc.

You do not need to supply 3 or more items in each list - if you can only think of less, delete the unused items or leave them blank.

The third section (C) is to get an idea of what role(s) best describe how you see yourself in the Bitcoin Cash ecosystem. We will not ask you for personally identifying information, and you can submit this survey form anonymously to us.


How to submit this survey
-------------------------

There are various ways:

If you wish complete anonymity:
- save your response in an online text dump service (suggestions: https://hastebin.com or https://glot.io/new/plaintext) and convey the URL of your saved response to anyone in the BCHN project with instructions to forward it on to freetrader. You can also send it to freetrader via any known channel that you trust - this can be email to freetrader at tuta dot io, or Memo (https://memo.cash/topic/BCHN+PCS+input), or raising an Issue on Gitlab (https://gitlab.com/bitcoin-cash-node/bchn-public-consultation-system/) to register your response, or DM to freetrader in Bitcoin Cash Node slack (bitcoincashnode.slack.com) or via the telegram or freenode IRC bridges to BCHN slack). See https://bitcoincashnode.org for ways to get in touch with the project. With the methods listed, you'd need to use a burner email or pseudonymous profile to submit your response.

If you don't mind associating your response with an already known identity:
- use any of the aforementioned submission channels with a known profile. Additionally, you can also PM your response to u/ftrader or submit it in #general-discuss on Bitcoin Cash Node slack.

Since we have no good way to verify role claims from anonymous submissions, associating verifiably with your known online identity may help us prioritize. We promise, to the very best of our efforts, to keep your identity association confidential.

Submission deadline
-------------------

This survey is active until end of March 2020, but if you submit your feedback ASAP that would help us include it for consideration in our funding proposal planning.

we intend to conduct future surveys similar to this one to calibrate our project's course.

`%< --- BEGIN feedback section --- you can cut your reply here ---`

Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

It can be anything: technical improvement, organizational, directional...

1.
2.
3.
...
(add more items as you need)


Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

Generally things that affect the whole protocol / currency / ecosystem,
and not BCHN specifically.

1.
2.
3.
...
(add more items as you need)


Section C - Information about yourself
--------------------------------------
This will help us to better assess the input.
Please put an 'X' or and 'x' into whichever of the following applies:

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive

- [ ] - a Bitcoin Cash pool operator or executive

- [ ] - a Bitcoin Cash exchange operator or executive

- [ ] - a Bitcoin Cash payment service provider operator or executive

- [ ] - a Bitcoin Cash protocol or full node client developer

- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)

- [ ] - an executive of a retail business using Bitcoin Cash

- [ ] - a person with significant amounts of their long term asset holdings in BCH

- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash

- [ ] - a BCH user

- [ ] - other - please specify: ....................................

`>% --- END feedback section --- you can cut your reply here ---`
